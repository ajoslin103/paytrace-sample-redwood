const process = require('process')

const path = require('path')
const __file = path.basename(__filename)

import { nanoid } from 'nanoid'
import md5 from 'md5'

const got = require('got')
const axios = require('axios')
const stringify = require('json-stringify-safe')

// https://www.prisma.io/docs/reference/api-reference/prisma-client-reference
import { db } from 'src/lib/db'

import { thenDebug } from 'src/lib/thenables'
import { logger } from 'src/lib/logger'

const PayTraceEndpoint = 'https://api.paytrace.com'

const PayTraceOAuthEndpoint = `${PayTraceEndpoint}/oauth/token`
const PayTraceProtectEndpoint = `${PayTraceEndpoint}/v1/payment_fields/token/create`
const PayTraceSaleEndpoint = `${PayTraceEndpoint}/v1/transactions/sale/pt_protect`

const PayTraceUsername = process.env.PAYTRACE_LOGIN
const PayTracePassword = process.env.PAYTRACE_PASSWORD
const PaytraceIntegratorId = process.env.PAYTRACE_INTEGRATOR_ID

interface IPayTraceToken {
  access_token: string
  token_type: string
  expires_in: number
  created_at: number
}

let PayTraceToken: IPayTraceToken = {
  access_token: '',
  token_type: '',
  expires_in: 0,
  created_at: 0,
}

const tokenExpired = (token): boolean => {
  const { expires_in, created_at } = token
  return Date.now() / 1000 > expires_in + created_at
}

// --------------------------------------------------------------------------------
const updateOAuthToken = async (): Promise<void> => {
  try {
    const { body } = await got.post(PayTraceOAuthEndpoint, {
      json: {
        grant_type: 'password',
        username: PayTraceUsername,
        password: PayTracePassword,
      },
      responseType: 'json',
    })
    PayTraceToken = { ...body } // sideEffect, I know...
  } catch (err) {
    logger.error(`[${__file}] ~ getOAuthToken: ${err}`)
    return Promise.reject(new Error(`failed to login to PayTrace`))
  }
}

// --------------------------------------------------------------------------------
const getOAuthToken = async (): Promise<string> => {
  if (tokenExpired(PayTraceToken)) {
    await updateOAuthToken()
  }
  const { access_token } = PayTraceToken
  return access_token
}

// --------------------------------------------------------------------------------
// --------------- called to setup the form
export const paytraceClientKey = async () => {
  const bearerToken = await getOAuthToken()
  const endpointOpts: any = {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
      'Content-Type': 'text/plain',
    },
    responseType: 'json',
  }
  // logger.debug(
  //   `[${__file}] ~ paytraceClientKey endpoint: ${PayTraceProtectEndpoint} with bearerToken: ${bearerToken} and endpointOpts: ${stringify(
  //     endpointOpts,
  //     null,
  //     2
  //   )}`
  // )
  return axios
    .post(PayTraceProtectEndpoint, bearerToken, endpointOpts)
    .then((result) => (result.data ? result.data : result))
    .catch((err) => {
      logger.error(
        `[${__file}] paytraceClientKey() axios threw: ${stringify(
          err.response.data,
          null,
          2
        )}`
      )
      return err.response.data
    })
}

// --------------------------------------------------------------------------------
// --------------- called to submit the payment
export const paytraceSubmitPayment = async ({ input }) => {
  const bearerToken = await getOAuthToken()
  const endpointOpts: any = {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
      'Content-Type': 'application/json',
    },
    responseType: 'json',
  }
  const { hpf_token, enc_key, seatingTotal, monthlyTotal } = input
  const payTraceRequest = {
    amount: seatingTotal + monthlyTotal,
    hpf_token,
    enc_key,
    integrator_id: PaytraceIntegratorId,
  }
  // logger.debug(
  //   `[${__file}] ~ paytraceSubmitPayment endpoint: ${PayTraceSaleEndpoint} with bearerToken: ${bearerToken} endpointOpts: ${stringify(
  //     endpointOpts,
  //     null,
  //     2
  //   )}, and body: ${stringify(payTraceRequest, null, 2)}`
  // )
  return axios
    .post(PayTraceSaleEndpoint, payTraceRequest, endpointOpts)
    .then((result) => (result.data ? result.data : result))
    .catch((err) => {
      logger.error(
        `[${__file}] paytraceSubmitPayment axios threw: ${stringify(
          err.response.data,
          null,
          2
        )}`
      )
      return err.response.data
    })
}

// --------------------------------------------------------------------------------

// const payTraceResponseCodes = {
//   [1]: 'One more errors has occurred',
//   [100]: 'Your password was successfully updated',
//   [101]: 'Your transaction was successfully approved',
//   [102]: 'Your transaction was not approved',
//   [103]: 'Your transaction was successfully approved',
//   [106]: 'Your transaction was successfully refunded',
//   [107]: 'Your transaction was not successfully refunded',
//   [108]:
//     'Your TEST transaction was successfully refunded NO FUNDS ACTUALLY REFUNDED',
//   [109]: 'Your transaction was successfully voided',
//   [110]: 'Your transaction was not successfully voided',
//   [112]: 'Your transaction was successfully captured',
//   [113]: 'Your transaction was not successfully captured',
//   [120]: 'Your check was successfully processed',
//   [122]: 'Your check was successfully refunded',
//   [124]: 'Your check was successfully managed',
//   [125]: 'Your check was NOT successfully processed',
//   [149]:
//     'The receipt for transaction ID ######## was successfully emailed to email address',
//   [150]: 'The recurring transaction was successfully created',
//   [151]: 'The recurring transaction was successfully updated',
//   [152]: 'The recurring transaction was successfully deleted',
//   [155]:
//     'The customers most recent recurring transaction took place on DD/MM/YYYY',
//   [160]:
//     'The customer profile for customer ID/customer Name was successfully created',
//   [161]:
//     'The customer profile for customer ID/customer Name was successfully updated',
//   [162]:
//     'The customer profile for customer ID/customer Name was successfully deleted',
//   [165]:
//     'Your transaction was successfully approved and customer profile is created',
//   [167]:
//     'Your transaction was successfully approved but the customer profile already existed',
//   [170]: 'Visa level 3 was successfully added to Transaction ID ',
//   [171]: 'MasterCard level 3 was successfully added to Transaction ID',
//   [175]:
//     'The batch was successfully initiated, and the batch report will be sent in just a few moments',
//   [180]: 'The transaction amount was successfully adjusted',
// }

export const schema = gql`
  type PaytraceClientKey {
    clientKey: String!
  }

  type PaytracePaymentResult {
    success: Boolean!
    response_code: Int!
    status_message: String!
    transaction_id: Int!
    approval_code: String!
    approval_message: String!
    avs_response: String!
    csc_response: String!
    external_transaction_id: Int!
  }

  input PaytracePaymentRequest {
    hpf_token: String!
    enc_key: String!
    seatingTotal: Float!
    monthlyTotal: Float!
    roundedMonths: Int!
    name: String!
    seats: Int!
    start: DateTime!
    stop: DateTime!
  }

  type Query {
    paytraceClientKey: PaytraceClientKey @skipAuth
    paytraceSubmitPayment(
      input: PaytracePaymentRequest!
    ): PaytracePaymentResult @skipAuth
  }
`

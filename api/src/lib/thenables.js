
const stringify = require('json-stringify-safe');

import { logger } from './logger'

export const thenDebug = (label) => (data) => {
  logger.debug(`${label}: ${stringify(data, null, 2)}`)
  return data
}

export const thenError = (label) => (resJSON) => {
  if (resJSON.error && resJSON.error.statusCode > 300) {
    logger.error(`Error: in [${label}], statusCode: ${resJSON.error.statusCode}, ${stringify(resJSON.error, null, 2)}`)
    throw new Error(JSON.stringify(resJSON.error)) // whatever we throw will be .toString()'d -- it will arrive in error.message
  }
  return resJSON
}

export const errorOnNull = (label) => (data) => {
  if (!data) {
    return new Error(label)
  }
  return data
}


// https://material-ui.com/components/steppers/
import { Container } from '@material-ui/core'

import clsx from 'clsx'

import EventPurchaseForm from '../../forms/EventPurchaseForm/EventPurchaseForm'
import useSharedClasses from 'src/hooks/shared-styles'

const PurchasePage = ({ choice }) => {
  const classes = useSharedClasses()

  return (
    <Container className={clsx(classes.flexContainer, classes.receiptMaxWidth)}>
      <EventPurchaseForm choice={choice} />
    </Container>
  )
}

export default PurchasePage

import { render } from '@redwoodjs/testing'

import PurchasePage from './PurchasePage'

describe('PurchasePage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<PurchasePage />)
    }).not.toThrow()
  })
})

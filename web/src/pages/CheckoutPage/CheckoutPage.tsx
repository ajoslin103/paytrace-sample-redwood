// https://material-ui.com/components/steppers/
import { Container } from '@material-ui/core'

import clsx from 'clsx'

import useScript from '../../hooks/use-script.js'
const paytraceProtectJS = 'https://protect.paytrace.com/js/protect.min.js'
import useSharedClasses from 'src/hooks/shared-styles'
import PaytraceFormCell from 'src/cells/PaytraceFormCell'
import Receipt from 'src/components/Receipt'

const CheckoutPage = () => {
  const classes = useSharedClasses()
  useScript(paytraceProtectJS)

  return (
    <Container className={clsx(classes.flexCenter, classes.marginEverything)}>
      <Container className={clsx(classes.flexContainer, classes.flexWrap)}>
        <Container className={clsx(classes.recieptSmall, classes.flexCenter)}>
          <Receipt />
        </Container>
        <Container className={clsx(classes.flexCenter)}>
          <PaytraceFormCell />
        </Container>
      </Container>
    </Container>
  )
}

export default CheckoutPage

export const formatPhone = (alleged: string | number): string => {
  alleged = (alleged || '').toString()
  alleged = alleged.replace(/[^0-9]/g, '')
  alleged = alleged.replace(/[-_]/g, '')
  alleged += '__________'
  let entered = alleged.slice(0, 3) + '-'
  entered += alleged.slice(3, 6) + '-'
  entered += alleged.slice(6, 10)
  entered = entered.replace(/[-_]+$/, '')
  return entered
}

export const packPhoneForDB = (alleged: string | number): string => {
  return `${alleged}`.replace(/[^0-9]/g, '')
}

export const extractPhone = (alleged: string | number): string => {
  alleged = (alleged || '').toString()
  const digits = alleged.replace(/[^0-9]/g, '')
  let extension = digits.slice(-4)
  let prefix = digits.slice(-7, -4)
  let area = digits.slice(-10, -7)
  let entered = `${area}-${prefix}-${extension}`
  return entered
}

export const formatCurrency = (alleged: string | number): string =>
  new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(
    +alleged
  )

export const formatNumber = (alleged: string | number): string =>
  new Intl.NumberFormat().format(+alleged)

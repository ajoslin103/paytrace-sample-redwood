// error: {"graphQLErrors":[{"message":"Unexpected token o in JSON at position 1","extensions":{"exception":{}}}]}

import { parse, format } from 'date-fns'

// ------------------------
// ------------------------

const fixDateString = (aDate) => format(parse(aDate), 'MM/DD/YYYY')

const investigateAndFix = (obj) => {
  if (obj === null) {
    return obj
  }
  if (typeof obj !== 'object') {
    return obj
  }
  try {
    Object.keys(obj).forEach((prop) => {
      if (prop === 'start' || prop === 'stop') {
        obj[prop] = fixDateString(obj[prop])
      }
      return investigateAndFix(obj[prop])
    })
    return obj
  } catch (err) {
    console.error(`error processing obj of type: ${typeof obj}`, obj)
  }
}

export const fixStartAndStopDateStrings = (givenObj) => {
  const fixedObj = investigateAndFix(givenObj)
  return fixedObj
}

// ------------------------
// ------------------------

export const extractError = (fromThis) => {
  if (typeof fromThis === 'string') return fromThis
  if (fromThis.message) return fromThis.message
  if (fromThis.graphQLErrors) return fromThis.graphQLErrors[0].message
  return fromThis
}

// ------------------------
// ------------------------

// input: {}, keys: []
export const filterInputs = (input: any, allowed: string[]) => {
  return Object.keys(input).reduce((r, e) => {
    if (allowed.includes(e)) {
      if (['start', 'stop'].includes(e)) {
        r[e] = new Date(input[e])
      } else {
        r[e] = input[e]
      }
    }
    return r
  }, {})
}

// ------------------------
// ------------------------

export const buildEventInviteRegUrlFromPlanner = ({ routes, eventId }) => {
  const qrString = `${process.env.VACCESS_HOST}/attendee-registration/${eventId}`
  console.log(`buildEventInviteRegUrl ~ qrString`, qrString)
  return qrString
}

// ------------------------
// ------------------------

export const buildForwardInviteRegUrlFromPlanner = ({ routes, attendeeId }) => {
  const qrString = `${process.env.VACCESS_HOST}/forward-registration/${attendeeId}`
  console.log(`buildEventInviteRegUrl ~ qrString`, qrString)
  return qrString
}

// ------------------------
// ------------------------

export const getErrorMessage = (input: any) => {
  const allegedError = input.message || input
  let allegedMsg = allegedError.message || allegedError
  try {
    allegedMsg = JSON.parse(allegedMsg)
    allegedMsg = allegedMsg.message || allegedMsg
  } catch (err) {
    console.error(`getErrorMessage ~ err`, err)
  }
  return allegedMsg
}

// ------------------------
// ------------------------

export const shouldhideNavMenu = (_isAuthenticated, _pathname) => {
  return true
}

// ------------------------
// ------------------------

export const shouldHideAvatarMenu = (isAuthenticated, pathname) => {
  return true
  // const onRegistration = /\/attendee-registration\//.test(pathname)
  // const onAttendee = /\/attendee\//.test(pathname)
  // const onAttendeeOwner = /\/attendee-owner\//.test(pathname)
  // const onGuest = /\/guest\//.test(pathname)
  // const onEventPass = /\/event-pass\//.test(pathname)

  // return !isAuthenticated || (onRegistration || onAttendee || onAttendeeOwner || onGuest || onEventPass)
}

// ------------------------
// ------------------------

export const hideLoginOutButton = (isAuthenticated, pathname) => {
  const onRegistration = /\/attendee-registration\//.test(pathname)
  const onAttendee = /\/attendee\//.test(pathname)
  const onAttendeeOwner = /\/attendee-owner\//.test(pathname)
  const onGuest = /\/guest\//.test(pathname)
  const onEventPass = /\/event-pass\//.test(pathname)

  return (
    !isAuthenticated ||
    onRegistration ||
    onAttendee ||
    onAttendeeOwner ||
    onGuest ||
    onEventPass
  )
}

// ------------------------
// ------------------------

export const numeric = (val) => +`${val}`.replace(/[^0123456789]/g, '')

// ------------------------
// ------------------------

export const clamp = (min, val, max) =>
  Math.max(min, Math.min(numeric(val), max))

// ------------------------
// ------------------------

export const hasVaccessProof = (recordData) => recordData.answerStatus // attendee or forward

// ------------------------
// ------------------------

export const isAnswered = (answerData) =>
  answerData.proofRequired ? !!answerData.image?.name : answerData.answer // attendee or forward

// ------------------------
// ------------------------

export const answersVsInquiries = (attendeeForwardOrGuest) => {
  const inquiries = attendeeForwardOrGuest?.event?.inquiries || []
  const answers = attendeeForwardOrGuest?.answers || []
  return answersInUse(inquiries, answers)
}

// ------------------------
// ------------------------

export const answersInUse = (inquiries, answers) => {
  const answersWithInquiry = []
  const answersWithoutInquiry = []

  if (inquiries.length && answers.length) {
    inquiries.forEach((inquiry) => {
      const answer = answers.find((a) => inquiry.id === a.inquiryId)
      if (inquiry) {
        answersWithInquiry.push(answer)
      } else {
        answersWithoutInquiry.push(answer)
      }
    })
  }

  return [answersWithInquiry, answersWithoutInquiry]
}

// ------------------------
// ------------------------

export const answersReduction = (answers) =>
  answers
    .filter((e) => !!e)
    .map((e) => (e.proofRequired ? !!e.image?.name : e.answer))
    .reduce((r, e) => (r = r || e), false)

// ------------------------
// ------------------------

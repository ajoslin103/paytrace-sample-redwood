import { clamp } from 'src/lib/utils'
import { parse, format } from 'date-fns'

export const seatingToText = (seats: number) => {
  if (seats > 10000) {
    return { label: 'Tier 4', descriptive: '10,000+ Attendees' }
  }
  if (seats > 5000) {
    return { label: 'Tier 3', descriptive: '5,000 - 10,000 Attendees' }
  }
  if (seats > 1000) {
    return { label: 'Tier 2', descriptive: '1,000 - 5,000 Attendees' }
  }
  return { label: 'Tier 1', descriptive: '1 - 1,000 Attendees' }
}

export const seatingToCost = (seats: number) => {
  if (seats > 10000) {
    return { perSeat: 0.75, perMonth: 69.95 }
  }
  if (seats > 5000) {
    return { perSeat: 1.5, perMonth: 59.95 }
  }
  if (seats > 1000) {
    return { perSeat: 2.0, perMonth: 49.95 }
  }
  return { perSeat: 3.0, perMonth: 39.95 }
}

export const choiceToSeats = (choice: string) => {
  switch (choice) {
    case 'tier4': {
      return 12500
    }
    case 'tier3': {
      return 7500
    }
    case 'tier2': {
      return 2500
    }
    default: {
      return 500
    }
  }
}

export const EVENT_MAX = 150000
export const bePositive = (v: any) =>
  clamp(0, +`${v}`.replace(/[^0-9]/g, ''), EVENT_MAX)

export const calcTotal = (start: any, seats: any, stop: any) => {
  const from = Date.parse(start)
  const to = Date.parse(stop)
  const duration = Math.abs(to - from)
  const months = duration / 1000 / 60 / 60 / 24 / 30
  const roundedMonths = Math.floor(1 + months)
  console.debug(`roundedMonths: ${roundedMonths}`) // eslint-disable-line no-console
  const { perSeat, perMonth } = seatingToCost(bePositive(seats))
  return perSeat * bePositive(seats) + perMonth * roundedMonths
}

export const getRoundedMonths = (start: Date, stop: Date) => {
  const to = stop.getTime()
  const from = start.getTime()
  const duration = Math.abs(to - from)
  const months = duration / 1000 / 60 / 60 / 24 / 30
  const roundedMonths = Math.floor(1 + months)
  return roundedMonths
}

export interface ICalculations {
  perSeat: number
  perMonth: number
  descriptive: string
  label: string
  seatingTotal: number
  monthlyTotal: number
  total: number
  roundedMonths: number
}

export const doCalculations = (
  startStr: string,
  seats: any,
  stopStr: string
): ICalculations => {
  const roundedMonths = getRoundedMonths(parse(startStr), parse(stopStr))
  console.debug(`roundedMonths: ${roundedMonths}`) // eslint-disable-line no-console
  const { perSeat, perMonth } = seatingToCost(bePositive(seats))
  const { descriptive, label } = seatingToText(bePositive(seats))
  const seatingTotal = perSeat * bePositive(seats)
  const monthlyTotal = perMonth * roundedMonths
  const total = seatingTotal + monthlyTotal
  return {
    perSeat,
    seatingTotal,
    monthlyTotal,
    perMonth,
    descriptive,
    label,
    total,
    roundedMonths,
  }
}

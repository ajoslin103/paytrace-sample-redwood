import { render } from '@redwoodjs/testing'

import EventPurchaseForm from './EventPurchaseForm'

describe('EventPurchaseForm', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<EventPurchaseForm />)
    }).not.toThrow()
  })
})

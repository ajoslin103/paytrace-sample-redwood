// https://material-ui.com/components/switches/
import { Box, Button, Container, Grid, Typography } from '@material-ui/core'

import { parse, format } from 'date-fns'

// https://www.npmjs.com/package/react-hook-form-mui
// https://material-ui-pickers.dev/api/DateTimePicker
import {
  DatePickerElement,
  FormContainer,
  TextFieldElement,
} from 'react-hook-form-mui'

// copied from inside 'react-hook-form-mui'
import DateDayjsProvider from '../DateDayjsProvider'

// https://github.com/dohomi/react-hook-form-mui/blob/master/stories/FormContainer.stories.tsx#L18
import { useFormContext, useWatch } from 'react-hook-form'

import clsx from 'clsx'

import { navigate, routes } from '@redwoodjs/router'

import useSharedClasses from 'src/hooks/shared-styles'
import NormalDialog from 'src/containers/NormalDialog'
import PaytraceFormCell from 'src/cells/PaytraceFormCell'
import Receipt from 'src/components/Receipt'

import { updateState } from 'src/states/purchasing'

export interface EventPurchaseFormValues {
  name: string
  seats: number
  start: Date
  stop: Date
}

interface EventPurchaseFormProps {
  submitName?: string
  choice: string
}

export const EventPurchaseForm = ({
  choice = '',
  submitName = 'Purchase',
}: EventPurchaseFormProps) => {
  const classes = useSharedClasses()

  const [isPurchasing, setIsPurchasing] = React.useState(false)

  const donePurchasing = () => {
    setIsPurchasing(false)
  }

  const startPurchasing = () => {
    setIsPurchasing(true)
    navigate(routes.checkout())
  }

  const onSubmit = (values: any) => {
    console.debug(`onSubmit - values: ${JSON.stringify(values, null, 2)}`)
  }

  const onCancel = () => {
    console.debug(`onCancel: `)
    navigate(routes.home())
  }

  const today = new Date().toLocaleDateString()
  const formDefaults: EventPurchaseFormValues = {
    name: 'My Event',
    start: parse(today),
    stop: parse(today),
    seats: 100,
  }

  const formDiv = React.useRef<HTMLDivElement>(null)
  React.useEffect(() => {
    console.debug('innerWidth', formDiv.current.clientWidth)
  })

  const ConditionalSubmit = () => {
    const { setValue } = useFormContext()
    const [name, allegedSeats, allegedStart, allegedStop] = useWatch({
      name: ['name', 'seats', 'start', 'stop'],
    })
    const seats = +`${allegedSeats}`
      .split('')
      .filter((e) => '0123456789'.includes(e))
      .join('')
    const start = allegedStart || today
    const stop = allegedStop || today
    const disabled = !name || !seats
    setTimeout(() => {
      if (allegedSeats != seats) {
        setValue('seats', seats)
      }
      updateState({ name, seats, stop, start })
    })
    return (
      <Button
        onClick={startPurchasing}
        variant="contained"
        color="primary"
        type="submit"
        disabled={disabled}
      >
        {submitName}
      </Button>
    )
  }

  const checkKeyDown = (e) => {
    if (e.key === 'Enter') e.preventDefault()
  }

  return (
    <>
      <NormalDialog
        title="Vaccess Event Purchase"
        isOpen={isPurchasing}
        doClose={donePurchasing}
      >
        <Container className={classes.receiptDisplay}>
          <Container className={classes.recieptSmall}>
            <Receipt />
          </Container>
          <Container>
            <PaytraceFormCell />
          </Container>
        </Container>
      </NormalDialog>
      <FormContainer defaultValues={formDefaults} onSuccess={onSubmit}>
        <div
          onKeyDown={(e) => checkKeyDown(e)}
          className={clsx(classes.formRoot)}
          ref={formDiv}
        >
          <Box className={classes.comfortableWidth}>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <TextFieldElement
                  size="small"
                  margin="dense"
                  fullWidth={true}
                  name="name"
                  label="Event Name"
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <TextFieldElement
                  size="small"
                  margin="dense"
                  fullWidth={true}
                  name="seats"
                  type="number"
                  label="Expected Attendence"
                />
              </Grid>
            </Grid>
          </Box>
          <Box className={classes.comfortableWidth}>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <DateDayjsProvider>
                  <DatePickerElement
                    size="small"
                    margin="dense"
                    fullWidth={true}
                    strictCompareDates={true}
                    name="start"
                    minDate={today}
                    label="Event Begins"
                    required
                  />
                </DateDayjsProvider>
              </Grid>
              <Grid item xs={6}>
                <DateDayjsProvider>
                  <DatePickerElement
                    size="small"
                    margin="dense"
                    fullWidth={true}
                    strictCompareDates={true}
                    name="stop"
                    minDate={today}
                    label="Event Ends"
                    required
                  />
                </DateDayjsProvider>
              </Grid>
            </Grid>
          </Box>
          <Box className={classes.recieptResponsive}>
            <Receipt />
          </Box>
          <Grid container spacing={2}>
            <Grid item xs={6} className={classes.centeredText}>
              <ConditionalSubmit />
            </Grid>
            <Grid item xs={6} className={classes.centeredText}>
              <Button variant="contained" onClick={onCancel}>
                Cancel
              </Button>
            </Grid>
          </Grid>
        </div>
      </FormContainer>
    </>
  )
}

export default EventPurchaseForm

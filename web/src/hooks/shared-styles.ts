// https://material-ui.com/components/switches/
import { makeStyles, createStyles } from '@material-ui/core'

// https://www.npmjs.com/package/clsx
// import clsx from 'clsx'

export default makeStyles((theme) =>
  createStyles({
    // ------------------------
    agreement: {
      height: '33vh',
      overflowY: 'scroll',
      padding: theme.spacing(1),
      border: '1px solid grey',
      marginRight: theme.spacing(2),
      paddingRight: theme.spacing(2),
    },

    agreementAttendee: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      fontWeight: 'bold',
    },

    agreementHasGuests: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      fontWeight: 'bold',
    },

    actionsContainer: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(2),
    },

    appBarTitle: {
      color: 'white',
    },

    appBarContainer: {
      '& .MuiContainer-root': {
        display: 'flex',
      },
      display: 'flex',
    },

    appBarUsername: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },

    appBarTitleContainer: {
      display: 'flex',
      alignItems: 'flex-end',
      justifyContent: 'flex-start',
    },

    appBarVaccess: {
      flex: 1,
      [theme.breakpoints.between('xs', 'sm')]: {
        height: '4vh',
        maxWidth: '20vw',
      },
      [theme.breakpoints.between('sm', 'md')]: {
        height: '4vh',
        maxWidth: '20vw',
      },
      [theme.breakpoints.between('md', 'lg')]: {
        height: '3vh',
        maxWidth: '10vw',
      },
      [theme.breakpoints.between('lg', 'xl')]: {
        height: '3vh',
        maxWidth: '10vw',
      },
      backgroundSize: 'contain !important',
    },

    appBarVaccessPlanner: {
      flex: 1,
      [theme.breakpoints.between('xs', 'sm')]: {
        height: '4vh',
        maxWidth: '20vw',
      },
      [theme.breakpoints.between('sm', 'md')]: {
        height: '4vh',
        maxWidth: '20vw',
      },
      [theme.breakpoints.between('md', 'lg')]: {
        height: '5vh',
        maxWidth: '10vw',
      },
      [theme.breakpoints.between('lg', 'xl')]: {
        height: '5vh',
        maxWidth: '10vw',
      },
      backgroundSize: 'contain !important',
    },

    appBarByVaxxifi: {
      flex: 1,
      [theme.breakpoints.between('xs', 'sm')]: {
        height: '2vh',
        maxWidth: '14vw',
      },
      [theme.breakpoints.between('sm', 'md')]: {
        height: '2vh',
        maxWidth: '14vw',
      },
      [theme.breakpoints.between('md', 'lg')]: {
        height: '3vh',
        maxWidth: '7vw',
      },
      [theme.breakpoints.between('lg', 'xl')]: {
        height: '3vh',
        maxWidth: '7vw',
      },
      backgroundSize: 'contain !important',
    },

    appBarButtonContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },

    appBarButton: {
      flex: 1,
      [theme.breakpoints.between('xs', 'sm')]: {
        height: '2vh',
        maxWidth: '7vw',
      },
      [theme.breakpoints.between('sm', 'md')]: {
        height: '2vh',
        maxWidth: '7vw',
      },
      [theme.breakpoints.between('md', 'lg')]: {
        height: '2vh',
        maxWidth: '7vw',
      },
      [theme.breakpoints.between('lg', 'xl')]: {
        height: '2vh',
        maxWidth: '4vw',
      },
      [theme.breakpoints.up('md')]: {
        height: '2vh',
        maxWidth: '4vw',
      },
      backgroundSize: 'contain !important',
    },

    appBarButtonInline: {
      flex: 1,
      margin: 0,
      [theme.breakpoints.between('xs', 'sm')]: {
        height: '2vh',
        maxWidth: '6vw',
      },
      [theme.breakpoints.between('sm', 'md')]: {
        height: '2vh',
        maxWidth: '6vw',
      },
      [theme.breakpoints.between('md', 'lg')]: {
        height: '2vh',
        maxWidth: '6vw',
      },
      [theme.breakpoints.between('lg', 'xl')]: {
        height: '2vh',
        maxWidth: '3vw',
      },
      [theme.breakpoints.up('md')]: {
        height: '2vh',
        maxWidth: '3vw',
      },
      backgroundSize: 'contain !important',
    },

    // ------------------------
    bottomMargin: {
      marginBottom: theme.spacing(2),
    },

    buttonMargin: {
      margin: theme.spacing(1),
    },

    // ------------------------

    checkoutWrapper: {
      '& .MuiContainer-root': {
        display: 'flex',
      },
      justifyContent: 'center',
      display: 'flex',
    },

    cardDeckLarge: {
      padding: theme.spacing(2),
      margin: theme.spacing(2),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
    },

    cardDeckMedium: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
    },

    cardDeckSmall: {
      padding: theme.spacing(0),
      margin: theme.spacing(0),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
    },

    cardDeckLargeLeft: {
      padding: theme.spacing(2),
      margin: theme.spacing(2),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'left',
    },

    cardDeckMediumLeft: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'left',
    },

    cardDeckSmallLeft: {
      padding: theme.spacing(0),
      margin: theme.spacing(0),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'left',
    },

    cardDeckLargeRight: {
      padding: theme.spacing(2),
      margin: theme.spacing(2),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'right',
    },

    cardDeckMediumRight: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'lright',
    },

    cardDeckSmallRight: {
      padding: theme.spacing(0),
      margin: theme.spacing(0),
      display: 'flex !important',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'right',
    },

    centeredText: {
      textAlign: 'center',
    },

    centeredTextPadded: {
      paddingTop: theme.spacing(1),
      marginTop: theme.spacing(1),
      textAlign: 'center',
    },

    checkboxPadding: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
    },

    comfortableWidth: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      width: '100%',
    },

    comfortableWWidth: {
      padding: theme.spacing(2),
      margin: theme.spacing(2),
    },

    comfortableVWidth: {
      paddingTop: theme.spacing(1),
      marginTop: theme.spacing(1),
      width: '100%',
    },

    comfortable1: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
    },

    comfortable: {
      padding: theme.spacing(2),
      margin: theme.spacing(2),
    },

    comfortableCenter: {
      textAlign: 'center',
      padding: theme.spacing(2),
      margin: theme.spacing(2),
    },

    comfortableCenter1: {
      textAlign: 'center',
      padding: theme.spacing(1),
      margin: theme.spacing(1),
    },

    comfortableRight: {
      textAlign: 'right',
      paddingTop: theme.spacing(1),
      width: '100%',
    },

    comfortableR: {
      paddingRight: theme.spacing(1),
      marginRight: theme.spacing(1),
    },

    comfortableY: {
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2),
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
    },

    comfortableX: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },

    comfortableWidthCheckboxes: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      width: '100%',
    },

    comfortableLocationWidth: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      width: '100%',
    },

    checkboxGroup: {
      display: 'flex',
    },

    card: {
      minWidth: 275,
      maxWidth: 275,
      padding: theme.spacing(2),
      margin: theme.spacing(2),
    },

    cardContainer: {
      padding: theme.spacing(2),
      margin: theme.spacing(2),
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
    },

    containBackground: {
      backgroundSize: 'contain !important',
    },

    // ------------------------
    dropDownRoot: {
      display: 'flex',
    },

    dropDownButton: {
      marginRight: theme.spacing(2),
    },

    // ------------------------
    fiftyPercent: {
      width: '50vw',
      height: '40vh',
    },

    fourtyHigh: {
      height: '40vh',
    },

    fiftyThirtyPercent: {
      width: '30vw',
      height: '40vh',
    },

    fiftyThirtyPercentCentered: {
      textAlign: 'center',
      width: '100%',
      height: '30vh',
    },

    fileInfoBox: {
      textAlign: 'center',
      padding: theme.spacing(2),
      margin: theme.spacing(2),
      height: 200,
    },

    flexCenter: {
      '& .MuiContainer-root': {
        display: 'flex',
      },
      justifyContent: 'center',
    },

    flexColumn: {
      flexDirection: 'column',
    },

    flexCenteredColumn: {
      flexDirection: 'column',
    },

    flexDisplay: {
      display: 'flex',
    },

    flexContainer: {
      '& .MuiContainer-root': {
        display: 'flex',
      },
      margin: 'auto',
    },

    flexGrowNoSelect: {
      webkitUserSelect: 'none',
      mozUserSelect: '-moz-none',
      msUserSelect: 'none',
      userSelect: 'none',
      flexGrow: 1,
    },

    flexGrow: {
      flexGrow: 1,
    },

    flexWrap: {
      flexWrap: 'wrap',
    },

    fontRegular1Rem: {
      fontWeight: theme.typography.fontWeightRegular,
    },

    fontRegular1RemCenter: {
      textAlign: 'center',
      fontWeight: theme.typography.fontWeightRegular,
    },

    formRoot: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      paddingRight: theme.spacing(4),
      margin: theme.spacing(1),

      '& .MuiTextField-root': {
        margin: theme.spacing(1),
      },
      '& .MuiButtonBase-root': {
        margin: theme.spacing(1),
      },
    },

    fourFifthsHeight: {
      height: '80%',
      margin: theme.spacing(1),
      width: '50%',
    },

    fullWidthCentered: {
      paddingTop: theme.spacing(1),
      marginTop: theme.spacing(1),
      width: '100%',
      textAlign: 'center',
    },

    fullHeight: {
      height: '100%',
    },

    fullHeightAndWidth: {
      height: '100%',
      width: '100%',
    },

    fullWidth: {
      width: '100%',
    },

    fullWidth1Rem: {
      width: '100%',
    },

    // ------------------------
    highFive: {
      zIndex: 5,
    },

    // ------------------------
    icon: {
      color: theme.palette.text.secondary,
      marginRight: theme.spacing(2),
    },

    // ------------------------
    leftText: {
      textAlign: 'left',
    },

    lessWidth: {
      width: '66%',
    },

    leftPad: {
      paddingLeft: theme.spacing(1),
    },

    // ------------------------
    loginSignup: {
      marginTop: '3rem',
      marginLeft: 'auto',
      marginRight: 'auto',
      paddingBottom: '1rem',
      maxWidth: 500,
    },

    // ------------------------
    marginEverything: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },

    // ------------------------
    notReading: {
      height: '5vh',
    },

    // ------------------------

    oneHalfWideCentered: {
      width: '50vw',
      textAlign: 'center',
    },

    oneThirdWideCentered: {
      width: '30vw',
      textAlign: 'center',
    },

    oneThirdHeight: {
      height: '30vh',
      margin: theme.spacing(1),
    },

    opacity40: {
      opacity: 0.4,
    },

    // ------------------------
    paperColor: {
      backgroundColor: theme.palette.background.paper,
    },

    paymentId: {
      padding: theme.spacing(1),
      margin: theme.spacing(1),
      color: 'darkgrey',
    },

    payTraceContainer: {
      '& .MuiContainer-root': {
        display: 'flex',
      },
      flexDirection: 'column',
      padding: theme.spacing(1),
      width: '355px',
    },

    popperInfoBox: {
      textAlign: 'left',
      padding: theme.spacing(3),
      margin: theme.spacing(3),
      height: 100,
    },

    // ------------------------
    reading: {
      height: '25vh',
    },

    receiptDisplay: {
      '& .MuiContainer-root': {
        display: 'flex',
      },
    },

    receiptMaxWidth: {
      maxWidth: 800,
      width: 'auto',
    },

    receipt: {
      display: 'flex',
      flexDirection: 'column',
      minWidth: 500,
      maxWidth: 500,
      padding: theme.spacing(2),
      margin: theme.spacing(2),
    },

    receiptEventName: {
      width: 300,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      textAlign: 'right',
    },

    receiptLine: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },

    receiptPayTraceLogo: {
      width: '150px',
      height: '30px',
    },

    receiptSubTotalFirst: {
      marginTop: theme.spacing(1),
    },

    receiptSubTotalLast: {
      borderBottom: '1px solid gray',
    },

    receiptSubTotal: {
      marginBottom: theme.spacing(1),
    },

    receiptGrandTotal: {
      fontStyle: 'bold',
    },

    recieptResponsive: {
      [theme.breakpoints.up('xs')]: {
        zoom: 0.45,
      },
      [theme.breakpoints.up('sm')]: {
        zoom: 0.7,
      },
      [theme.breakpoints.up('md')]: {
        zoom: 0.8,
      },
      [theme.breakpoints.up('lg')]: {
        zoom: 1,
      },
    },

    recieptSmall: {
      [theme.breakpoints.up('xs')]: {
        zoom: 0.45,
      },
      [theme.breakpoints.up('sm')]: {
        zoom: 0.6,
      },
      [theme.breakpoints.up('md')]: {
        zoom: 0.7,
      },
      paddingBottom: theme.spacing(2),
    },

    remoteImageCentered: {
      display: 'flex',
      flexDirection: 'row',
      alignContent: 'center',
      justifyContent: 'center',
    },

    rightText: {
      textAlign: 'right',
    },

    resetContainer: {
      padding: theme.spacing(3),
    },

    reset1Container: {
      padding: theme.spacing(3),
      marginLeft: '-50px',
    },

    // ------------------------
    s3Image: {
      padding: theme.spacing(2),
      height: '40vh',
      width: '60vw',
    },

    slimTop: {
      marginTop: theme.spacing(1),
    },

    stepperTitle: {
      padding: theme.spacing(3),
    },

    // ------------------------
    thirtyPercentCentered: {
      textAlign: 'center',
      height: '30vh',
    },

    topRight1: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },

    twoThirdsHigh: {
      height: '66vh',
    },

    twoThirdsWide: {
      width: '60vw',
    },

    twoThirdsWideCentered: {
      width: '60vw',
      textAlign: 'center',
    },

    threeQuarterHigh: {
      height: '75vh',
    },

    // ------------------------
    vaccessFont: {
      color: '#549e39',
      fontWeight: theme.typography.fontWeightRegular,
    },

    vaccessFontCenter: {
      color: '#549e39',
      textAlign: 'center',
      fontWeight: theme.typography.fontWeightRegular,
    },

    version: {
      position: 'absolute',
      bottom: '20px',
      right: '20px',
      color: 'darkgrey',
    },

    verticalSpace: {
      paddingTop: theme.spacing(2),
      marginTop: theme.spacing(2),
    },

    // ------------------------
    white: {
      color: 'white',
    },

    // ------------------------
  })
)

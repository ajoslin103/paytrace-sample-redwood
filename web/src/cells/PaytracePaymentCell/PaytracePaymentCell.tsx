// https://material-ui.com/components/switches/
import { Typography } from '@material-ui/core'

import { parse } from 'date-fns'

// import { navigate, routes } from '@redwoodjs/router'

import Progress from 'src/components/Progress'
// import useSharedClasses from 'src/hooks/shared-styles'

export const beforeQuery = (props) => {
  const variables = { ...props }
  variables.input.start = parse(props.input.start)
  variables.input.stop = parse(props.input.stop)
  return { variables, fetchPolicy: 'no-cache' }
}

export const QUERY = gql`
  query PaytracePaymentQuery($input: PaytracePaymentRequest!) {
    paytraceSubmitPayment(input: $input) {
      success
      response_code
      status_message
      transaction_id
      approval_code
      approval_message
      avs_response
    }
  }
`

export const Empty = () => null

export const Loading = () => {
  return (
    <>
      <Progress />
      <Typography variant="body2">Charging your card...</Typography>
    </>
  )
}

export const Failure = ({ error }) => (
  <Typography variant="body2" color="error">
    Error: {error.message}
  </Typography>
)

export const Success = ({ paytraceSubmitPayment }) => {
  const { status_message, approval_message } = paytraceSubmitPayment
  return (
    <>
      <Typography variant="body2">{status_message}</Typography>
      <Typography variant="body2">{approval_message}</Typography>
    </>
  )
}

/**
response.data
  "success":true,
  "response_code":101,
  "status_message":"Your transaction was successfully approved.",
  "transaction_id":105968532,
  "approval_code":"TAS569",
  "approval_message":"ADDRESS MATCH - Approved and completed",
  "avs_response":"Address Match Only",
  "csc_response":"",
  "external_transaction_id":""
err.response.data
  "success": false,
  "response_code": 102,
  "status_message": "Your transaction was not approved.",
  "transaction_id": 434101963,
  "approval_code": "",
  "approval_message": "DECLINED         - Insufficient funds",
  "avs_response": "",
  "csc_response": "Unknown",
  "external_transaction_id": ""
*/
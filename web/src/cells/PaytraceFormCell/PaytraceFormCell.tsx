// import type { FindPaytraceQuery } from 'types/graphql'
// import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

// https://lostpebble.github.io/pullstate/
import { useStoreState } from 'pullstate'

import { PurchasingInfo } from 'src/states/purchasing'

// https://material-ui.com/components/switches/
import { Box, Button, Card, Container, Typography } from '@material-ui/core'

import clsx from 'clsx'

import { navigate, routes } from '@redwoodjs/router'
import { toast, Toaster } from '@redwoodjs/web/toast'

import PaytracePaymentCell from 'src/cells/PaytracePaymentCell'

import useSharedClasses from 'src/hooks/shared-styles'
import { doCalculations } from 'src/lib/event-calculations'

export const beforeQuery = (props) => {
  const variables = { ...props }
  return { variables, fetchPolicy: 'no-cache' }
}

export const QUERY = gql`
  query FindPaytraceQuery {
    paytraceClientKey {
      clientKey
    }
  }
`

export const Empty = () => null
export const Loading = () => <Typography variant="body2">Loading...</Typography>
export const Failure = ({ error }) => (
  <Typography variant="body2" color="error">
    Error: {error.message}
  </Typography>
)

export const Success = ({ paytraceClientKey }) => {
  const { clientKey } = paytraceClientKey
  const classes = useSharedClasses()

  const purchaseInfo = useStoreState(PurchasingInfo)
  const [submitting, setSubmitting] = React.useState(false)
  const [notCompleted, setNotCompleted] = React.useState(true)
  const [paytraceSubmission, setPaytraceSubmission] = React.useState<any>({})

  const { start, seats, stop } = purchaseInfo
  const { seatingTotal, monthlyTotal, roundedMonths } = doCalculations(
    start,
    seats,
    stop
  )

  React.useEffect(() => {
    globalThis.PTPayment.setup({
      authorization: { clientKey },
    }).then(function (instance) {
      const smaller = {
        width: '75vw',
      }
      globalThis.PTPayment.style({
        cc: smaller,
      })
    })
  })

  const handleCancel = (evt) => {
    setTimeout(() => {
      navigate(routes.purchase())
    })
  }

  const handleSubmit = (evt) => {
    if (evt.key === 'Enter') {
      evt.preventDefault()
    }

    // The tokenization process also validates the sensitive data payment fields
    globalThis.PTPayment.process() //call tokenization
      .then((r) => submitPayment(r))
      // submit nonce and cipher key to PayTrace Protect Servers to tokenize sensitive data payment fields
      .catch((err) => handleError(err))
    // handle unsuccessful tokenization failures
  }

  const handleError = (err) => {
    console.debug(
      `[PaytraceFormCell.tsx] ~ handleError() err: ${JSON.stringify(
        err,
        null,
        2
      )}`
    )

    const please = err.reason
      .map((e) => e.description)
      .filter((e) => /Please/.test(e))

    const only = [...new Set(please)]

    only.forEach((e: string) => toast.error(e))
  }

  const submitPayment = (data: any) => {
    console.debug(
      `[PaytraceFormCell.tsx] ~ submitPayment() data: ${JSON.stringify(
        data,
        null,
        2
      )}`
    )

    if (data.success) {
      const ccData = { ...data.message }
      const submission = {
        ...ccData,
        ...purchaseInfo,
        seatingTotal,
        monthlyTotal,
        roundedMonths,
      }

      console.debug(
        `[PaytraceFormCell.tsx] ~ submission data: ${JSON.stringify(
          submission,
          null,
          2
        )}`
      )

      setPaytraceSubmission(submission)
      setNotCompleted(false)
      setSubmitting(true)
    }
  }

  /*
    [PaytraceFormCell.tsx] ~ submitPayment() data: {
    "success": true,
    "message": {
      "hpf_token": "cb34d8fa-28bc-40a1-9248-6d2002e1c888",
      "enc_key": "CpjnJLcvGs-kGQR23tyA8G9RdATiycT4SdQ1eUnF3Ns="
    }
  */

  return (
    <Card className={classes.payTraceContainer} elevation={4}>
      <Container
        className={clsx(classes.payTraceContainer, classes.flexCenter)}
      >
        <Toaster
          toastOptions={{
            position: 'bottom-center',
            className: 'rw-toast',
            duration: 6000,
          }}
        />
        {notCompleted && <div id="pt_hpf_form" />}
        {submitting ? (
          <PaytracePaymentCell input={paytraceSubmission} />
        ) : (
          <Container className={classes.flexCenter}>
            {notCompleted && (
              <>
                <Box className={classes.comfortable1}>
                  <Button
                    onClick={handleSubmit}
                    variant="contained"
                    type="submit"
                    color="primary"
                  >
                    Submit
                  </Button>
                </Box>
                <Box className={classes.comfortable1}>
                  <Button
                    type="button"
                    onClick={handleCancel}
                    variant="contained"
                  >
                    Cancel
                  </Button>
                </Box>
              </>
            )}
          </Container>
        )}
      </Container>
    </Card>
  )
}

/* eslint no-param-reassign: ["error", { "props": false }] */

// https://lostpebble.github.io/pullstate/
import { Store } from 'pullstate'

const today = new Date().toLocaleDateString()

export interface IPurchasingInfo {
  name: string
  seats: number
  start: string
  stop: string
}

export const PurchasingInfo = new Store<IPurchasingInfo>({
  name: '',
  seats: 0,
  start: today,
  stop: today,
})

export const updateState = ({ name, seats, start, stop }: IPurchasingInfo) => {
  // console.debug(`updateState seats: ${seats}`)
  PurchasingInfo.update((s) => {
    s.name = name
    s.seats = +seats
    s.start = start
    s.stop = stop
  })
}

import { render } from '@redwoodjs/testing'

import Receipt from './Receipt'

describe('Receipt', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<Receipt />)
    }).not.toThrow()
  })
})

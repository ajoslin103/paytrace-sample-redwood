// https://material-ui.com/components/typography/
import {
  Box,
  Card,
  CardContent,
  Container,
  Typography,
} from '@material-ui/core'

import clsx from 'clsx'
import { parse, format } from 'date-fns'

// https://lostpebble.github.io/pullstate/
import { useStoreState } from 'pullstate'

import { PurchasingInfo } from 'src/states/purchasing'

import useSharedClasses from 'src/hooks/shared-styles'
import { doCalculations } from 'src/lib/event-calculations'
import { formatCurrency, formatNumber } from 'src/lib/value-formatters'

const Receipt = () => {
  const classes = useSharedClasses()

  const {
    name,
    seats,
    start: startStr,
    stop: stopStr,
  } = useStoreState(PurchasingInfo, (s) => ({
    name: s.name,
    seats: s.seats,
    start: s.start,
    stop: s.stop,
  }))

  const start = format(parse(startStr), 'MM/DD/YYYY')
  const stop = format(parse(stopStr), 'MM/DD/YYYY')

  const {
    perSeat,
    seatingTotal,
    monthlyTotal,
    perMonth,
    descriptive,
    label,
    total,
    roundedMonths,
  } = doCalculations(start, seats, stop)

  const today = new Date().toLocaleDateString()
  const now = new Date().toLocaleTimeString()

  // console.debug(
  //   JSON.stringify(
  //     {
  //       today,
  //       now,
  //       name,
  //       start,
  //       stop,
  //       seats,
  //       perSeat,
  //       seatingTotal,
  //       label,
  //       perMonth,
  //       roundedMonths,
  //       monthlyTotal,
  //       total,
  //     },
  //     null,
  //     2
  //   )
  // )

  return (
    <Card className={classes.receipt} elevation={4}>
      <CardContent>
        <Box className={clsx(classes.bottomMargin, classes.centeredText)}>
          <Typography variant="h4">Vaccess, by Vaxxifi</Typography>
          <Typography variant="h6">
            {today} {now}
          </Typography>
        </Box>
        <Container className={classes.receiptLine}>
          <Typography align="left" variant="h6">
            Event Name
          </Typography>
          <Typography className={classes.receiptEventName} variant="h5">
            {name}
          </Typography>
        </Container>
        <Container className={classes.receiptLine}>
          <Typography align="left" variant="h6">
            Event Start
          </Typography>
          <Typography align="right" variant="h5">
            {start}
          </Typography>
        </Container>
        <Container className={classes.receiptLine}>
          <Typography align="left" variant="h6">
            Event Stop
          </Typography>
          <Typography align="right" variant="h5">
            {stop}
          </Typography>
        </Container>
        <Container
          className={clsx(classes.receiptLine, classes.receiptSubTotalFirst)}
        >
          <Typography align="left" variant="h6">
            Event Seats
          </Typography>
          <Typography align="right" variant="h5">
            {formatNumber(Math.abs(seats))}
          </Typography>
        </Container>
        <Container
          className={clsx(classes.receiptLine, classes.receiptSubTotalLast)}
        >
          <Typography align="left" variant="h6">
            Per Seat [{label}]
          </Typography>
          <Typography align="right" variant="h5">
            {formatCurrency(perSeat)}
          </Typography>
        </Container>
        <Container
          className={clsx(classes.receiptLine, classes.receiptSubTotal)}
        >
          <Typography align="left" variant="h6">
            Seating Subtotal
          </Typography>
          <Typography align="right" variant="h5">
            {formatCurrency(seatingTotal)}
          </Typography>
        </Container>
        <Container
          className={clsx(classes.receiptLine, classes.receiptSubTotalFirst)}
        >
          <Typography align="left" variant="h6">
            Per Month [{label}]
          </Typography>
          <Typography align="right" variant="h5">
            {formatCurrency(perMonth)}
          </Typography>
        </Container>
        <Container
          className={clsx(classes.receiptLine, classes.receiptSubTotalLast)}
        >
          <Typography align="left" variant="h6">
            Number of Months
          </Typography>
          <Typography align="right" variant="h5">
            {formatNumber(roundedMonths)}
          </Typography>
        </Container>
        <Container
          className={clsx(classes.receiptLine, classes.receiptSubTotal)}
        >
          <Typography align="left" variant="h6">
            Monthly Subtotal
          </Typography>
          <Typography align="right" variant="h5">
            {formatCurrency(monthlyTotal)}
          </Typography>
        </Container>
        <Container
          className={clsx(classes.receiptLine, classes.receiptGrandTotal)}
        >
          <Typography align="right" variant="h5">
            Purchase Total
          </Typography>
          <Typography variant="h4">{formatCurrency(total)}</Typography>
        </Container>
      </CardContent>
    </Card>
  )
}

export default Receipt

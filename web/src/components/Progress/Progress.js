import { LinearProgress } from '@material-ui/core';

export default () => {
  return (
    <LinearProgress color="primary" />
  )
}

import { render } from '@redwoodjs/testing'

import NormalDialog from './NormalDialog'

describe('NormalDialog', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<NormalDialog />)
    }).not.toThrow()
  })
})
